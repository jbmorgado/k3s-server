#!/bin/bash
GREEN='\033[0;32m'
LB='\033[1;34m' # light blue
NC='\033[0m' # No Color

MASTERS=$(echo $(multipass list | grep master | awk '{print $1}'))
WORKERS=$(echo $(multipass list | grep worker | awk '{print $1}'))
NODES+=$MASTERS
NODES+=' '
NODES+=$WORKERS
NODES_A=($NODES) # easier way to check size of string is to convert to an array

echo $NODES
if [ ${#NODES_A[@]} -ge 1 ]
then
    echo -e "[${LB}Info${NC}] found nodes: ${NODES}"
    echo -e "[${LB}Info${NC}] stop ${NODES}"
    multipass stop ${NODES}
    sleep 5
    echo -e "[${LB}Info${NC}] delete ${NODES}"
    multipass delete ${NODES}
    sleep 5
    echo -e "[${LB}Info${NC}] purge"
    multipass purge
else
    echo -e "[${LB}Info${NC}] no nodes found"
fi

echo -e "[${GREEN}FINISHED${NC}]"