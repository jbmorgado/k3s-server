#!/bin/bash
GREEN='\033[0;32m'
LB='\033[1;34m' # light blue
YL='\033[1;33m' # yellow
NC='\033[0m' # No Color

masterCount=1
nodeCount=2
cpuCount=2
memCount=4
diskCount=10

# MASTERS=$(echo "k3s-master") 
MASTERS=$(eval 'echo k3s-master-{0..'"$((masterCount-1))"'}')
WORKERS=$(eval 'echo k3s-worker-{0..'"$((nodeCount-1))"'}')
NODES+=$MASTERS
NODES+=' '
NODES+=$WORKERS

echo -e "[${YL}Warning${NC}] expects the existence of the multipass.yaml file with your RSA pub key"
echo -e "[${LB}Info${NC}] creating nodes ${NODES}"
for NODE in ${NODES}; do multipass launch --name ${NODE} --cpus ${cpuCount} --mem ${memCount}G --disk ${diskCount}G --cloud-init multipass.yaml; done

echo -e "[${LB}Info${NC}] cluster VMs:"
multipass ls

MASTER_IP=$(echo $(multipass list | grep master-0 | awk '{print $3}'))
SEC_MASTERS_IP=$(echo $(multipass list | grep -E 'master-[1-9]+' | awk '{print $3}'))
WORKERS_IP=$(echo $(multipass list | grep worker | awk '{print $3}'))

echo -e "[${LB}Info${NC}] installing k3s on master"
k3sup install --ip ${MASTER_IP} --user ubuntu --k3s-extra-args "--cluster-init --no-deploy traefik --node-taint=node-role.kubernetes.io/master=effect:NoSchedule"

echo -e "[${LB}Info${NC}] setting environment variables for kubectl:"
export KUBECONFIG=/home/morgado/k3s-server/kubeconfig
kubectl config set-context default 

echo -e "[${LB}Info${NC}] joining workers to master"
for WORKER_IP in ${WORKERS_IP}
do
    k3sup join --ip ${WORKER_IP} --user ubuntu --server-ip ${MASTER_IP} --server-user ubuntu
done

sleep 10
echo -e "[${LB}Info${NC}] add node label to workers"
for WORKER in ${WORKERS}
do
    kubectl label node ${WORKER} node-role.kubernetes.io/node= > /dev/null
done

echo -e "[${LB}Info${NC}] install Traefik"
helm repo add traefik https://helm.traefik.io/traefik
helm repo update
# kubectl create ns traefik-v2
# helm install --namespace=traefik-v2 traefik traefik/traefik
helm install traefik traefik/traefik

# expose dashboard at http://127.0.0.1:9000/dashboard/
kubectl port-forward $(kubectl get pods --selector "app.kubernetes.io/name=traefik" --output=name) 9000:9000

kubectl get all -A

echo -e "[${GREEN}FINISHED${NC}]"
